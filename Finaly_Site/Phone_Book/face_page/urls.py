from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from .views import *

urlpatterns = [
    path("", face_page),
    path("input/", input_page, name='input'),
    path("redakt/<int:id>", redakt_page, name='redakt'),
    path("redakt/<int:id>/delete", delete_page, name='delete'),
    path('autocomplete/', autocomplete, name="autocomplete"),
    path('input_num/', input_num, name="input_num"),
    path('redakt_num/', csrf_exempt(redakt_num), name="redakt_num")
]
