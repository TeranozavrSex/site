
           const phoneInputField = document.querySelector("#id_phone");
           const phoneInput = window.intlTelInput(phoneInputField, {
             utilsScript:
               "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/utils.js",
           });

           a = " iti-0__item-";
           a += document.getElementById(code_val).dataset.countryCode;
           document.querySelector(".iti__selected-flag").setAttribute('aria-activedescendant',a);

           a = "iti__flag iti__"
           a +=  document.getElementById(code_val).dataset.countryCode;
           document.querySelector(".iti__flag").className = a;

           document.getElementById(code_val).className += " iti__active";


           var h = "";
           id_phone =  document.getElementById('id_phone');
           const phoneMask = IMask((id_phone), {mask: id_phone.value});
           id_phone.value = phone_val;

             $( ".iti--allow-dropdown" ).click(function( event ) {
                if (id_phone.placeholder == ""){
                    ph = id_phone.value;
                    id_phone.placeholder = id_phone.value;
                }else{
                    ph = id_phone.placeholder;
                }
                h = "";
                for (i in ph){
                   if (parseInt(ph[i])){
                        h+="0";
                   }
                   else{
                        h += ph[i];
                   }
                }
                phoneMask.updateOptions({mask: h});
                phoneMask.updateValue()
             })

              $( ".iti__selected-flag" ).click(function( event ) {
                    id_phone.value = "";
              })


              function color_red5s(obj){
               var orig = obj.style.borderColor;
               obj.style.borderColor = 'red';
               setTimeout(function(){
                    obj.style.borderColor = orig;
               }, 3000);
             }

             var error = `<div class="error text-center" id="error"></div>`;
             form_name = document.querySelector('.form_name');
             form_phone = document.querySelector('.form_phone');
             form_email = document.querySelector('.form_email');

            $( ".ok" ).click(function( event ) {
                if(document.getElementById('error') != null){
                    document.getElementById('error').remove();
                }

                id_name.style.borderColor ="";
                id_phone.style.borderColor ="";
                id_email.style.borderColor ="";

                if( id_name.value.length < 1 ){
                    form_name.insertAdjacentHTML('beforeend', error);
                    document.querySelector('.error').textContent = "введите корректное имя";
                    color_red5s(id_name)
                }
                else if( id_phone.value.length != phoneMask.mask.length ){
                    form_phone.insertAdjacentHTML('beforeend', error);
                    document.querySelector('.error').textContent = `введите номер в формате выбранной страны пример: ${id_phone.placeholder}`;
                    color_red5s(id_phone)
                }
                else if (!id_email.validity.valid){
                    form_email.insertAdjacentHTML('beforeend', error);
                    document.querySelector('.error').textContent = "введите корректный номер почты";
                    color_red5s(id_email)
                }
                else{
                    document.querySelector('.ok').href = ".."
                    $.ajax({
                        url: redaktLink,
                        type: "PUT",
                        dataType: 'json',
                        data: JSON.stringify({id: id_num,
                               code: document.querySelector('.iti__active').id,
                               name: document.querySelector('#id_name').value,
                               phone: document.querySelector('#id_phone').value,
                               email: document.querySelector('#id_email').value,
                         }),
                        success: function(data){

                        }
                    });
                }
           })

            $( "#id_name" ).blur(function( event ) {
                if(document.getElementById('error') != null){
                    document.getElementById('error').remove();
                }
                if( id_name.value.length < 1 ){
                    form_name.insertAdjacentHTML('beforeend', error);
                    document.querySelector('.error').textContent = "введите корректное имя";
                    color_red5s(id_name)
                }else{
                    id_name.style.borderColor ="";
                }
           })

           $( "#id_phone" ).blur(function( event ) {
                if(document.getElementById('error') != null){
                    document.getElementById('error').remove();
                }
                if( id_phone.value.length != phoneMask.mask.length ){
                    form_phone.insertAdjacentHTML('beforeend', error);
                    document.querySelector('.error').textContent = `введите номер в формате выбранной страны пример: ${id_phone.placeholder}`;
                    color_red5s(id_phone)
                }else{
                    id_phone.style.borderColor ="";
                }

           })

           $( "#id_email" ).blur(function( event ) {
               if(document.getElementById('error') != null){
                        document.getElementById('error').remove();
               }
              if (!id_email.validity.valid){
                    form_email.insertAdjacentHTML('beforeend', error);
                    document.querySelector('.error').textContent = "введите корректный номер почты";
                    color_red5s(id_email)
              }else{
                    id_email.style.borderColor ="";
              }
           })