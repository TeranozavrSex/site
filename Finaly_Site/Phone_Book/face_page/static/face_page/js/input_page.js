       const phoneInputField = document.querySelector("#id_phone");
       const phoneInput = window.intlTelInput(phoneInputField, {
         utilsScript:
           "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/utils.js",
       });

       const phoneMask = IMask((id_phone), {mask: ""});
       id_phone =  document.getElementById('id_phone');
       id_email =  document.getElementById('id_email');
       id_name =  document.getElementById('id_name');

         $( ".iti--allow-dropdown" ).click(function( event ) {
            ph = id_phone.placeholder;
            let h = "";
            for (i in ph){
                console.log(ph[i])
               if (parseInt(ph[i])){
                    h+="0";
               }
               else{
                    h += ph[i];
               }
            }
            phoneMask.updateOptions({mask: h});
            phoneMask.updateValue()
         })

          $( ".iti__selected-flag" ).click(function( event ) {
                id_phone.value = "";
          })


            function color_red5s(obj){
               var orig = obj.style.borderColor;
               obj.style.borderColor = 'red';
               setTimeout(function(){
                    obj.style.borderColor = orig;
               }, 3000);
             }

             var error = `<div class="error text-center" id="error"></div>`;
             form_name = document.querySelector('.form_name');
             form_phone = document.querySelector('.form_phone');
             form_email = document.querySelector('.form_email');


           $( ".ok" ).click(function( event ) {
                if(document.getElementById('error') != null){
                    document.getElementById('error').remove();
                }

                id_name.style.borderColor ="";
                id_phone.style.borderColor ="";
                id_email.style.borderColor ="";

                if( id_name.value.length < 2 ){
                    form_name.insertAdjacentHTML('beforeend', error);
                    document.querySelector('.error').textContent = "введите корректное имя";
                    color_red5s(id_name)
                }
                else if( id_phone.value.length != id_phone.placeholder.length ){
                    form_phone.insertAdjacentHTML('beforeend', error);
                    document.querySelector('.error').textContent = "введите номер в формате выбранной страны";
                    color_red5s(id_phone)
                }
                else if (!id_email.validity.valid){
                    form_email.insertAdjacentHTML('beforeend', error);
                    document.querySelector('.error').textContent = "введите корректный номер почты";
                    color_red5s(id_email)
                }
                else{
                    document.querySelector('.ok').href = ".."
                    $.ajax({
                    url: saveLink,
                    type: 'post',
                    dataType: 'json',
                    data: {code: document.querySelector('.iti__active').id,
                           name: document.querySelector('#id_name').value,
                           phone: document.querySelector('#id_phone').value,
                           email: document.querySelector('#id_email').value,
                           csrfmiddlewaretoken: $('[name=csrfmiddlewaretoken]').val()
                     },
                    success: function(data){}
                    });
                }
           })



                       $( "#id_name" ).blur(function( event ) {
                if(document.getElementById('error') != null){
                    document.getElementById('error').remove();
                }
                if( id_name.value.length < 1 ){
                    form_name.insertAdjacentHTML('beforeend', error);
                    document.querySelector('.error').textContent = "введите корректное имя";
                    color_red5s(id_name)
                }else{
                    id_name.style.borderColor ="";
                }
           })

           $( "#id_phone" ).blur(function( event ) {
                if(document.getElementById('error') != null){
                    document.getElementById('error').remove();
                }
                if( id_phone.value.length != phoneMask.mask.length ){
                    form_phone.insertAdjacentHTML('beforeend', error);
                    document.querySelector('.error').textContent = `введите номер в формате выбранной страны пример: ${id_phone.placeholder}`;
                    color_red5s(id_phone)
                }else{
                    id_phone.style.borderColor ="";
                }

           })

           $( "#id_email" ).blur(function( event ) {
               if(document.getElementById('error') != null){
                        document.getElementById('error').remove();
               }
              if (!id_email.validity.valid){
                    form_email.insertAdjacentHTML('beforeend', error);
                    document.querySelector('.error').textContent = "введите корректный номер почты";
                    color_red5s(id_email)
              }else{
                    id_email.style.borderColor ="";
              }
           })

