
let a = 0;
let colpost= 0;
let checkpost = 0;

ajax(a, ' ')
a += 50;

function getContactListHTML(list){
  let contactList ='';
  for (item in list) {
    contactList += getContactListItemHTML(list[item]);
    colpost += 1;
  }
  checkpost = colpost % 50;
  if (colpost != 0 && checkpost == 0){
    document.getElementById('more').style.visibility = "visible";
  }
  else{
    document.getElementById('more').style.visibility = "hidden";
  }
  return contactList
}

function getContactListItemHTML(i){
  return `<li class="list-group-item contacts_items">
          <div>${ i.fields.name }</div>
          <div>${ i.fields.phone }</div>
          <div>${ i.fields.email }</div>
          <div class="buttons">
            <a class="float-right btn btn-primary btn-sm" href="redakt/${i.pk}">Редактировать</a>
          </div>
        </li>`
}

function ajax(a,eventData){
  $.ajax({
    url: '/autocomplete' ,                              /* Куда пойдет запрос */
    method: 'get',                                                /* Метод передачи (post или get) */
    dataType: 'json',                                             /* Тип данных в ответе (xml, json, script, html). */
    data: {term: eventData, count: a},                            /* Параметры передаваемые в запросе. */
    success: function(data){                                      /* функция которая будет выполнена после успешного запроса.  */
      if(JSON.parse(data).length == 0){
        document.getElementById('list').innerHTML = `<p class="text-center">Ни одного контакта не найдено :(((</p>`;
        getContactListHTML([]);
      }
      else if (a == 0){
        document.getElementById('list').innerHTML=getContactListHTML(JSON.parse(data));
      }else{
        document.getElementById('list').innerHTML+=getContactListHTML(JSON.parse(data));
      }
    }
  });
}


function throttle(func, ms){

  let isThrottled = false;
  let saveArgs;
  let saveThis;

  function wrapper(){

    if(isThrottled){
      saveArgs = arguments;
      saveThis = this;
      return;
    }

    func.apply(this, arguments)

    isThrottled = true;

    setTimeout(function() {
      isThrottled = false;
      if(saveArgs){
        wrapper.apply(saveThis, saveArgs)
        saveArgs = saveThis = null;
      }
    }, ms)
  }
  return wrapper;
}

const throttledAjax = throttle(ajax, 2000);

$( "#search" ).keyup(function( event ) {
  a = 0;
  colpost = 0;
  checkpost = 0;
  throttledAjax(a ,event.target.value);
  a += 50;
})

$( "#more" ).click(function( event ) {
  ajax(a, $( "#search" ).val())
  a += 50;
})

