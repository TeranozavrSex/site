from django import forms

from .models import Telephone


class PhoneForm(forms.ModelForm):
    class Meta:
        model = Telephone
        fields = ['name', 'code', 'phone', 'email']
        widgets = {
            "name": forms.TextInput(attrs={'class': 'form-control'}),
            "phone": forms.TextInput(attrs={'class': 'form-control', 'id': 'id_phone', 'type': 'tel'}),
            "email": forms.TextInput(attrs={'class': 'form-control',
                                            'pattern': '[A-Za-z0-9_-]{1,25}@[A-Za-z0-9_-]{1,15}\.[A-Za-z0-9_-]{1,10}'})
        }
