import json

from django.db.models import Q
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.views.generic import DeleteView

from .froms import PhoneForm
from .models import Telephone

from django.core import serializers


def face_page(request):
    base = Telephone.objects.order_by('name')
    return render(request, 'face_page/main_page.html', {'base': base})


def input_page(request):
    form = PhoneForm()
    return render(request, 'face_page/input_page.html', {'form': form})


def redakt_page(request, id):
    redakt_num = Telephone.objects.get(id=id)
    if request.method == "POST":
        form = PhoneForm(request.POST)
        if form.is_valid():
            redakt_num.phone = form.cleaned_data['phone']
            redakt_num.name = form.cleaned_data['name']
            redakt_num.email = form.cleaned_data['email']

            redakt_num.save()
            return redirect("/")
    elif request.method == "GET":
        form = PhoneForm(instance=redakt_num)
    return render(request, 'face_page/redakt_page.html', {'form': form, 'id': id})


def delete_page(request, id):
    redakt_num = Telephone.objects.get(id=id)
    redakt_num.delete()
    return redirect('/')


def autocomplete(request):
    i = int(request.GET.get('count'))
    if request.GET.get('term') == ' ':
        qs = Telephone.objects.all()[i: i + 50]
    else:
        q = request.GET.get('term')
        qs = Telephone.objects.filter(
            Q(name__icontains=q) | Q(phone__icontains=q) | Q(email__icontains=q)
        )[i: i + 50]
    return JsonResponse(serializers.serialize('json', qs), safe=False)


def input_num(request):
    code = request.POST.get('code')
    name = request.POST.get('name')
    phone = request.POST.get('phone')
    email = request.POST.get('email')
    form = PhoneForm(request.POST)

    form.phone = '+' + code + phone
    form.name = name
    form.email = email
    form.code = code
    form.save()
    return redirect("/")


def redakt_num(request):
    data = json.load(request)
    form = Telephone.objects.get(id=data['id'])

    form.phone = data['phone']
    form.name = data['name']
    form.email = data['email']
    form.code = data['code']
    form.save()
    return redirect("/")
