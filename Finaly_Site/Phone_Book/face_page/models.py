from django.db import models


class Telephone(models.Model):
    name = models.CharField(max_length=25)
    code = models.CharField(max_length=30)
    phone = models.CharField(max_length=25)
    email = models.CharField(max_length=40)

    def __str__(self):
        return f'{self.pk} - {self.name} -  {self.code} - {self.phone} - {self.email}'
