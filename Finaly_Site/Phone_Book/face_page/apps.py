from django.apps import AppConfig


class FacePageConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'face_page'
